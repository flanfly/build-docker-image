FROM debian:buster-slim

RUN apt update -y -qq && \
    apt install -y -qq --no-install-recommends \
        rustc cargo git curl clang make pkg-config nettle-dev libssl-dev ca-certificates capnproto libsqlite3-dev && \
    apt clean && \
    rm -fr /var/lib/lists/* /var/cache/* /usr/share/doc/* /usr/share/locale/*
